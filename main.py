from pm4py.algo.discovery.heuristics import variants
from pm4py.objects.log.importer.xes import importer as xes_importer
from pm4py.algo.discovery.heuristics import algorithm as heuristics_miner
from pm4py.objects.petri_net.exporter import exporter as pnml_exporter

from utils import display_graph, calculate_conformance


def main():
    log_file = 'InternationalDeclarations.xes'
    log = xes_importer.apply(log_file)

    net_classic, initial_marking_classic, final_marking_classic = heuristics_miner.apply(log, variant=variants.classic)
    heuristics_classic_results = {
        'net': net_classic,
        'initial_marking': initial_marking_classic,
        'final_marking': final_marking_classic
    }

    net_plus, initial_marking_plus, final_marking_plus = heuristics_miner.apply(log, variant=variants.plusplus)
    heuristics_plus_results = {
        'net': net_plus,
        'initial_marking': initial_marking_plus,
        'final_marking': final_marking_plus
    }

    print('Classic: ', calculate_conformance(log, heuristics_classic_results))
    print('Plus: ', calculate_conformance(log, heuristics_plus_results))

    pnml_exporter.apply(
        heuristics_classic_results.get('net'),
        heuristics_classic_results.get('initial_marking'),
        "classic_petri_net.pnml",
        heuristics_classic_results.get('final_marking')
    )
    pnml_exporter.apply(
        heuristics_plus_results.get('net'),
        heuristics_plus_results.get('initial_marking'),
        "plus_petri_net.pnml",
        heuristics_plus_results.get('final_marking')
    )

    display_graph(heuristics_classic_results)
    display_graph(heuristics_plus_results)


if __name__ == '__main__':
    main()
