import os
from sys import platform
from statistics import mean

from pm4py.visualization.petri_net import visualizer
from pm4py.conformance import conformance_diagnostics_token_based_replay


def display_graph(algorithm_results):
    graph = visualizer.apply(
        algorithm_results.get('net'),
        algorithm_results.get('initial_marking'),
        algorithm_results.get('final_marking')
    )

    try:
        visualizer.view(graph)
    except Exception as e:
        print(e)
        if platform == 'win32':
            print('Adding path to windows environment variables')
            os.environ['PATH'] += os.pathsep + 'C:/Program Files (x86)/Graphviz/bin/'
            visualizer.view(graph)


def calculate_conformance(log, algorithm_results):
    conformance_diag = conformance_diagnostics_token_based_replay(
        log,
        algorithm_results.get('net'),
        algorithm_results.get('initial_marking'),
        algorithm_results.get('final_marking'),
    )

    fitness_list = []
    for trace in conformance_diag:
        fitness_list.append(trace.get('trace_fitness'))

    return mean(fitness_list)
